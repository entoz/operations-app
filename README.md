## Operations App

A Laravel driven administration panel.

# Requirements

- Laravel 8.x
- MySQL (recommended) / PostgreSQL / SQLite / SQL Server

# Installation

- Configure your .ENV file with your database information;

# License

Operations App is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).