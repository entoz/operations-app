<div class="p-4 bg-gray text-white-super">
    {{ $name }}
</div>

<div class="p-4 text-sm text-center text-gray mt-4">{{ __('messages.delete?') }}</div>

<form method="DELETE" action="{{ $route }}" onsubmit="return window.submit(this)"
    data-callback="removeListItem" data-id="{{ $id }}">
    @csrf

    <div class="grid grid-cols-2 gap-4 m-4">
        <div>
            <button type="reset" class="w-full bg-white-x-light border-silver text-silver" onclick="modal.hide()">
                {{ __('app.cancel') }}
            </button>
        </div>
        <div>
            <button type="submit" class="w-full bg-red border-red">
                {{ __('app.delete') }}
            </button>
        </div>
    </div>

    <div class="message-window text-center text-sm"></div>
</form>
