@extends('layouts/app')

@section('title', __('app.' . $route) . ' / ' . __('app.title'))

@section('header')
    @include('parts.header')
@endsection

@section('aside')
    @include('parts.aside')
@endsection

@section('page-title')
    @include('parts.page-title', [
    'title' => __('app.' . ($item ? 'edit' : 'add')),
    'parent' => (isset($entryType) ? Str::plural($entryType->name) : __('app.' . $route)),
    'route' => route($route . '.index', (isset($entryType) ? [
    'entry_type' => $entryType->id
    ] : false))
    ])
@endsection

@section('content')
    <form method="post" class="form h-full"
        action="{{ $item ? route($route . '.update', isset($entryType) ? ['entry_type' => $entryType->id, Str::singular(Str::slug($route, '_')) => $item] : [Str::singular(Str::slug($route, '_')) => $item]) : route($route . '.store', isset($entryType) ? ['entry_type' => $entryType->id] : '') }}">
        <div class="flex h-full">
            <div class="flex-grow h-full overflow-y-auto overflow-x-hidden">
                <div class="container mx-auto p-6">
                    @include('parts.message-list')

                    @csrf

                    @if ($item)
                        @method('PATCH')

                        <input type="hidden" name="id" value="{{ $item->id }}" />
                    @else
                        @method('POST')
                    @endif
                    
                    @include('parts.fields')
                </div>
            </div>

            <div class="w-80 flex-shrink-0 p-6 border-l border-white-dark">
                <button type="submit" class="form-button">
                    {{ __('actions.save') }}
                </button>

                <button type="reset" class="form-button-alt mt-2"
                    onclick="document.location = '{{ route($route . '.index', isset($entryType) ? ['entry_type' => $entryType->id] : '') }}'; return false;">
                    {{ __('actions.cancel') }}
                </button>

                @if (isset($attributes) && is_array($attributes) && count($attributes) > 0)
                    @foreach ($attributes as $key => $attribute)
                        @if (isset($attribute['type']) && $attribute['type'] === 'boolean')
                            <div id="{{ $key }}-field-container" class="mt-6">
                                @include('parts.fields.' . $attribute['type'], [
                                    'id' => $key,
                                    'key' => $key,
                                    'name' => $key,
                                    'title' => __('app.' . Str::slug($key, '-')),
                                    'value' => $item->$key ?? '',
                                    'callback' => $attribute['callback'] ?? false,
                                    'data' => $attribute['data'] ?? false,
                                ])
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </form>
@endsection
