<div>
    @if ($fields && count($fields) > 0 && $languages && count($languages) > 0)
        <div class="tabs mt-10">
            <nav class="tab-nav flex flex-col sm:flex-row text-sm">
                @foreach ($languages as $language)
                    <button>
                        {{ $language->name }}

                        @if (count(preg_grep("/^meta." . $language->slug . "/", array_keys($errors->getMessages()))) > 0)
                            <div class="inline-block bg-red text-white-super text-xs w-4 rounded-full ml-2">
                                {{ count(preg_grep("/^meta." . $language->slug . "/", array_keys($errors->getMessages()))) }}
                            </div>
                        @endif
                    </button>
                @endforeach
            </nav>
            
            <div class="tab-content">
                @foreach ($languages as $language)
                    <div class="hidden">
                        <!--<div class="py-4 px-6 rounded bg-white-super border border-white">
                            <input type="checkbox" id="meta-status-{{ $language->slug }}" name="meta[status][{{ $language->slug }}]" value="1" />
                            <label for="meta-status-{{ $language->slug }}" class="ml-4">{{ __('app.enabled') }}</label>
                        </div>-->

                        <div id="{{ $language->slug }}-meta-fields">
                            @foreach ($fields as $field)
                                <div id="{{ $field->slug }}-field-container" class="mt-6">
                                    @include('parts.fields.' . $field['type'], [
                                        'id' => 'meta-' . $language->slug . '-' . $field->slug,
                                        'key' => 'meta.' . $language->slug . '.' . $field->slug,
                                        'name' => 'meta[' . $language->slug . '][' . $field->slug . ']',
                                        'title' => $field->name,
                                        'value' => $meta[$language->slug][$field->slug] ?? '',
                                        'callback' => $field['callback'] ?? false,
                                        'data' => $field['data'] ?? false,
                                    ])
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
