<div>
    <div class="modal fixed z-50 top-0 left-0 w-full h-full hidden">
        <div class="modal-mask absolute w-full h-full bg-gray-x-dark bg-opacity-50 flex">
            <div class="modal-wrapper p-0 w-full h-full md:h-auto md:w-3/4 lg:w-1/2 2xl:w-1/4 m-auto overflow-hidden bg-white-x-light shadow md:rounded-lg">
                <div class="modal-container relative w-full h-full">
                    <a href="#" role="button"
                        class="modal-close absolute top-2 right-2 rounded-md w-10 leading-9 pb-1 pr-1 text-center text-xl bg-gray-light hover:bg-gray-x-light text-white-super">
                        <span class="material-icons">close</span>
                    </a>

                    <div class="modal-content"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $('.modal').initModal()
        })
    </script>
</div>
