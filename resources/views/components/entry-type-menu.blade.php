<div>
    @if ($entryTypes)
        @foreach ($entryTypes as $entryTypeItem)
            <a href="{{ route('entries.index', ['entry_type' => $entryTypeItem->id]) }}"
                class="block p-4 {{ $entryType && $entryType->id == $entryTypeItem->id ? 'bg-white-x-light border-blue border-r-4' : 'text-gray-x-light' }}">
                {{ Str::plural($entryTypeItem->name) }}
            </a>
        @endforeach
    @endif
</div>