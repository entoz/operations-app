@extends('layouts/app')

@section('title', __('app.title'))

@section('header')
    @include('parts.header')
@endsection

@section('aside')
    @include('parts.aside')
@endsection

@section('page-title')
    @include('parts.page-title', ['title' => __('app.dashboard')])
@endsection

@section('content')
    <div class="flex h-full">
        <div class="flex-grow h-full overflow-y-auto overflow-x-hidden">
            <div class="p-8 text-silver">
                {{ config('app.name') }} / v{{ config('app.version') }}
            </div>
        </div>
    </div>
@endsection
