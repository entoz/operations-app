@extends('layouts/app')

@section('title', __('app.' . Str::plural($route)) . ' / ' . __('app.title'))

@section('header')
    @include('parts.header')
@endsection

@section('aside')
    @include('parts.aside')
@endsection

@section('page-title')
    @include('parts.page-title', [
    'title' => isset($entryType) ? Str::plural($entryType->name) : __('app.' . $route)
    ])
@endsection

@section('content')
    <div class="flex h-full">
        <div class="flex-grow h-full overflow-y-auto overflow-x-hidden">
            <div class="container mx-auto p-6">
                @include('parts.message-list')

                @if (isset($items) && count($items) > 0)
                    @include('parts.list.container', [
                        'route' => $route,
                        'hierarchical' => isset($hierarchical) && $hierarchical,
                        'sortable' => isset($sortable) && $sortable,
                        'parent' => 0,
                        'items' => $items,
                    ])
                @else
                    @include('parts.no-entry')
                @endif
            </div>
        </div>

        <div class="w-80 flex-shrink-0 p-6 border-l border-white-dark">
            @include('parts.list.actions')
        </div>
    </div>
@endsection
