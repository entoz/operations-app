@extends('layouts/app')

@section('title', __('app.title') . ' / ' . __('messages.not-found'))

@section('header')
    <div class="bg-white-super shadow">
        <div class="lg:container lg:mx-auto p-5">
            <h1 class="text-lg sm:text-3xl text-red">404 / {{ __('messages.not-found') }}</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="lg:container lg:mx-auto p-5">
        <p>{{ __('messages.the-page-you-are-looking-for-not-found') }}</p>
        <p>{{ __('messages.please-check-url-address') }}</p>
    </div>
@endsection

@section('footer')
    <div class="bg-white-x-dark">
        <div class="lg:container lg:mx-auto p-5 text-right text-silver">
            {{ __('app.title') . ' ' . __('messages.rights') . ' / ' . date('Y') }}
        </div>
    </div>
@endsection
