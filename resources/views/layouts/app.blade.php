<!DOCTYPE html>
<html lang="{{ Str::slug(app()->getLocale(), '-') }}">

<head>
    <meta charset="utf-8">
    <base href="{{ config('app.url') }}">
    <meta name="robots" content="NOODP,index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3">
    <meta name="Author" content="{{ __('app.title') }}" />
    <meta name="Copyright" content="{{ __('app.title') }} © {{ date('Y') }}" />
    <meta name="Description" content="{{ __('app.description') }}" />
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;900&display=swap" rel="stylesheet">
</head>

<body class="bg-white text-black">
    <div id="wrapper" class="flex flex-col h-screen overflow-x-hidden">
        <header class="flex bg-gray">@yield('header')</header>

        <div class="flex">
            <div class="w-60 flex-shrink-0 bg-white-light"></div>
            <div class="flex-grow py-5 px-8 text-gray bg-white-x-light">@yield('page-title')</div>
        </div>

        <main class="flex flex-grow min-h-0 bg-gray-400">
            <aside class="w-60 flex-shrink-0 bg-white-dark">@yield('aside')</aside>
            
            <article class="flex-grow">
                <div class="flex flex-col h-full">
                    @yield('content')
                </div>
            </article>
        </main>
    </div>
    
    <x-modal />

    <x-preloader />

    @stack('scripts')
</body>

</html>
