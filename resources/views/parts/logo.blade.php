<a href="{{ url('/') }}" id="logo"
    class="w-full inline-block p-4 text-center text-white-super bg-gray-dark hover:bg-gray-x-dark transition duration-200 ease-in-out">
    <div class="mark text-lg font-bold">{{ __('app.title') }}</div>
</a>
