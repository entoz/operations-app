<div class="bg-white-super rounded space-top overflow-hidden text-sm">
    <div class="bg-white-x-light text-silver text-xs p-4">{{ __('app.related-fields') }}</div>
    
    @if (!$availableFields->isEmpty())
        @foreach ($availableFields as $availableField)
            <div class="p-4 border-t border-white-dark">
                <input type="checkbox" id="related-field-{{ $availableField->id }}" name="related-field[]"
                    value="{{ $availableField->id }}" @if ($item && in_array($availableField->id, $item->fields->pluck('id')->all())) checked @endif />
                <label for="related-field-{{ $availableField->id }}" class="ml-4">{{ $availableField->name }}</label>
            </div>
        @endforeach
    @else
        <div class="p-4 border-t border-white-dark text-silver">{{ __('app.related-fields-not-found') }}</div>
    @endif
</div>