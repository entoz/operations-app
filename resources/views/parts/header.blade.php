<div class="w-60 flex-shrink-0">
    @include('parts.logo')
</div>

<div class="flex-grow text-right">
    @include('parts.settings-menu')
</div>
