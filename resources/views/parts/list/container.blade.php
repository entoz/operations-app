<ul class="list @if ($sortable) sortable @endif"
    data-parent="{{ $parent }}"
    data-route="{{ route('sort', ['route' => $route, 'parent' => $parent]) }}">
    @foreach ($items as $item)
        <li id="list-item-{{ $item->id }}" class="sortable-item" draggable="false" data-id="{{ $item->id }}">
            @if (isset($entryType))
                @include('parts.list.item-entry')
            @else
                @include('parts.list.item')
            @endif

            @if ($hierarchical && $item->children)
                @include('parts.list.container', [
                'route' => $route,
                'hierarchical' => $hierarchical,
                'sortable' => $sortable,
                'parent' => $item->id,
                'items' => $item->children,
                ])
            @endif
        </li>
    @endforeach
</ul>
