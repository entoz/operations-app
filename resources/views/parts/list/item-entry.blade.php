<div class="list-item">
    <span class="sortable-handle">=</span>

    <a class="text-gray hover:text-blue"
        href="{{ route($route . '.edit', ['entry_type' => $entryType->id, Str::singular(Str::slug($route, '_')) => $item]) }}">
        <?php echo $item->name ?? $item->slug; ?>
    </a>

    <a href="#" class="list-item-action"
        onclick="modal.show('{{ route('modal-view-entry-type-route-id', [
            'view' => 'delete',
            'route' => $route,
            'entry_type_id' => $entryType->id,
            'id' => $item->id,
            ]) }}'); return false">
        {{ __('app.delete') }}
    </a>
</div>
