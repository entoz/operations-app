<button class="form-button"
    onclick="document.location = '{{ route($route . '.create', isset($entryType) ? ['entry_type' => $entryType->id] : '') }}'">
    {{ __('app.add-new-' . Str::singular($route)) }}
</button>

@if (isset($items) && count($items) > 0 && isset($sortable) && $sortable)
    <div class="mt-6 rounded py-4 px-6 bg-white-x-light">
        <input type="checkbox" id="sortable-checkbox" />
        <label for="sortable-checkbox" class="pl-4 text-gray">{{ __('app.edit-order') }}</label>
    </div>
@endif
