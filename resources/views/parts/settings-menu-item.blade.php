<li>
    <a href="{{ $url }}"
        class="block p-4 border-t border-gray-x-light w-full min-w-max hover:bg-gray transition-backkground duration-300">
        <span class="material-icons mr-4">{{ $icon }}</span>
        {{ $name }}
    </a>
</li>
