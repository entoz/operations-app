<div id="message-body">
    @if (Session::has('message'))
        <div class="py-3 px-5 mb-6 border rounded border-green text-green">
            <span class="material-icons mr-4">check_circle</span>
            {{ Session::get('message') }}
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="py-3 px-5 mb-6 border rounded border-red text-red">
            <span class="material-icons mr-4">report_problem</span>
            {{ Session::get('warning') }}
        </div>
    @endif

    @if (Session::has('info'))
        <div class="py-3 px-5 mb-6 border rounded border-blue text-blue">
            <span class="material-icons mr-4">info</span>
            {{ Session::get('info') }}
        </div>
    @endif
</div>
