<div><label for="{{ $id }}">{{ $title }}</label></div>
<div class="mt-2">
    <input type="number" id="{{ $id }}" name="{{ $name }}" value="{{ old($key) ?? $value }}"
        class="form-input{{ $errors->has($name) ? ' wrong' : '' }}" {{ $errors->has($name) ? 'autofocus' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }} />

    @include('parts.fields.errors.warning')
</div>
