@if ($errors->has($key))
    <div class="form-input-warning">{{ $errors->first($key) }}</div>
@endif