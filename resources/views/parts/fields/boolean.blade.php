<div class="form-input px-6 py-4 bg-white-dark rounded border border-white{{ $errors->has($name) ? ' wrong' : '' }}">
    <input type="checkbox" id="{{ $id }}" name="{{ $name }}"
        {{ old($key) || (!old($key) && $value) ? 'checked' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }} value="1" />
    <label for="{{ $id }}" class="pl-4 text-gray">{{ $title }}</label>
</div>

@include('parts.fields.errors.warning')
