<div><label for="{{ $id }}">{{ $title }}</label></div>
<div class="mt-2">
    <textarea type="text" id="{{ $id }}" name="{{ $name }}"
        class="form-input{{ $errors->has($name) ? ' wrong' : '' }}" rows="5"
        {{ $errors->has($name) ? 'autofocus' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }}>{{ old($key) ?? $value }}</textarea>

    @include('parts.fields.errors.warning')
</div>
