<div><label for="{{ $id }}">{{ $title }}</label></div>
<div class="mt-2">
    <input type="datetime-local" id="{{ $id }}" name="{{ $name }}"
        value="{{ old($key) ?? ($value ? date('Y-m-d\TH:i', strtotime($value)) : date('Y-m-d\TH:i')) }}"
        class="form-input{{ $errors->has($name) ? ' wrong' : '' }}" {{ $errors->has($name) ? 'autofocus' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }} />

    @include('parts.fields.errors.warning')
</div>
