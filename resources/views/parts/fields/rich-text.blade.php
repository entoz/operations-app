<div><label for="{{ $id }}">{{ $title }}</label></div>
<div class="mt-2">
    <textarea type="text" id="{{ $id }}" name="{{ $name }}"
        class="rich-text form-input{{ $errors->has($name) ? ' wrong' : '' }}" rows="5"
        data-styles="{{ url('/') . '/css/editor.css' }}"
        {{ $errors->has($name) ? 'autofocus' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }}>{{ old($key) ?? $value }}</textarea>

    @include('parts.fields.errors.warning')
</div>

@once
    @push('scripts')
        <script src="https://cdn.tiny.cloud/1/{{ config('app.tinymce_api_key') }}/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    @endpush
@endonce
