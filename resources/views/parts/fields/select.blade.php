<div><label for="{{ $id }}">{{ $title }}</label></div>
<div class="mt-2">
    <select id="{{ $id }}" name="{{ $name }}"
        class="form-input{{ $errors->has($name) ? ' wrong' : '' }}" {{ $errors->has($name) ? 'autofocus' : '' }}
        {{ $callback ? 'data-callback=' . $callback : '' }}>
        @if ($data)
            @foreach ($data as $dataKey)
                <option value="{{ $dataKey }}"
                    {{ old($key) == $dataKey || (!old($key) && $value == $dataKey) ? 'selected' : '' }}>
                    {{ __('app.' . $dataKey) }}
                </option>
            @endforeach
        @endif
    </select>

    @include('parts.fields.errors.warning')
</div>
