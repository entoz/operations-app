@if (isset($attributes) && is_array($attributes) && count($attributes) > 0)
    @foreach ($attributes as $key => $attribute)
        @if (isset($attribute['type']) && !in_array($attribute['type'], ['boolean', 'hidden']))
            <div id="{{ $key }}-field-container" class="mb-6">
                @include('parts.fields.' . $attribute['type'], [
                'id' => $key,
                'key' => $key,
                'name' => $key,
                'title' => __('app.' . Str::slug($key, '-')),
                'value' => $item->$key ?? '',
                'callback' => $attribute['callback'] ?? false,
                'data' => $attribute['data'] ?? false,
                ])
            </div>
        @endif
    @endforeach
@endif

@if (isset($entryType))
    <x-meta-fields :entry="$item ? $item : new App\Models\Entry()" :entry-type="$entryType" />
@endif

@if (isset($availableFields))
    @include('parts.related-fields')
@endif
