<div class="h-full overflow-y-auto text-sm">
    <x-entry-type-menu :entry-type="isset($entryType) ? $entryType : false" />
</div>
