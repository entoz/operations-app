<div class="p-6 text-silver">
    {{ sprintf(__('messages.%s-not-found'), ucfirst(Str::plural(Str::slug($route, ' ')))) }}
</div>
