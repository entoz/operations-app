<ul class="text-white-super text-sm inline-block" role="navigation">
    <li class="dropdown">
        <a href="#">{{ __('app.settings') }}</a>
        <ul class="hidden">
            @include('parts.settings-menu-item', [
            'url' => route('languages.index'),
            'icon' => 'language',
            'name' => __('app.languages'),
            ])
            @include('parts.settings-menu-item', [
            'url' => route('options.index'),
            'icon' => 'settings',
            'name' => __('app.options'),
            ])
            @include('parts.settings-menu-item', [
            'url' => route('fields.index'),
            'icon' => 'view_agenda',
            'name' => __('app.fields'),
            ])
            @include('parts.settings-menu-item', [
            'url' => route('entry-types.index'),
            'icon' => 'assignment',
            'name' => __('app.entry-types'),
            ])
        </ul>
    </li>
</ul>
