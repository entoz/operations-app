@if (isset($parent) && $parent)
    <div class="inline-block text-xl mr-2">
        <a class="text-silver hover:text-gray font-thin" href="{{ $route }}">{{ $parent }}</a>
        <span class="text-silver ml-2">/</span>
    </div>
@endif

<h1 class="inline-block text-xl">{{ $title }}</h1>
