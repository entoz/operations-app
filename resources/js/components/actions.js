window.submit = function (form) {
    $.ajax({
        url: $(form).attr('action'),
        type: $(form).attr('method'),
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]', form).val()
        },
        contentType: 'application/json',
        data: $(form).serialize(),
        beforeSend: function () {
            helpers.showLoading('button[type=submit]', form)

            helpers.clearMessages(form)
        },
        complete: function () {
            helpers.hideLoading('button[type=submit]', form)
        },
        success: function (data) {
            if ($(form).data('callback') && typeof (window[$(form).data('callback')]) === 'function') {
                window[$(form).data('callback')](form);
            }
        },
        error: function (jqXHR) {
            helpers.showMessage(jqXHR.responseJSON.message, '.message-window', form, 'error');
        }
    })

    return false
}