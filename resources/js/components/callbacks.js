window.fieldTypeCallback = function (object) {
    var value = $(object).val()

    $('#regex-field-container').hide()
    $('#data-field-container').hide()

    if (value == 'select') {
        $('#data-field-container').show()
    } else if (value == 'string') {
        $('#regex-field-container').show()
    }
}

window.removeListItem = function (form) {
    $('#list-item-' + $(form).data('id')).fadeOut(300, function () { $(this).remove() })

    modal.hide()
}