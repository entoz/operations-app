import Sortable from 'sortablejs'

$(function () {
    if ($('.sortable').length) {
        $('.sortable').each(function () {
            Sortable.create(this, {
                group: 'nested',
                handle: '.sortable-handle',
                ghostClass: 'sortable-ghost',
                animation: 150,
                swapThreshold: 0.65,
                store: {
                    set: function (sortable) {
                        if (sortable.toArray().length) {
                            $.ajax({
                                url: $(sortable.el).data('route'),
                                type: 'PUT',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                contentType: 'application/json',
                                data: JSON.stringify({
                                    'ord': sortable.toArray()
                                }),
                                beforeSend: function () {
                                    window.preloader.count++;
                                    window.preloader.show()

                                    helpers.clearMessages('#message-body')
                                },
                                error: function (data) {
                                    helpers.showMessage(data.responseJSON.message, '#message-body', 'error');
                                },
                                complete: function () {
                                    window.preloader.count--;

                                    if (window.preloader.count == 0) {
                                        window.preloader.hide();
                                    }
                                }
                            })
                        }
                    }
                }
            })
        })

        if ($('#sortable-checkbox').length) {
            $('#sortable-checkbox').on('change', function () {
                $('.list').toggleClass('sortable-state', $(this).prop('checked'))
            })
        }
    }
})