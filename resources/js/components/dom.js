$(function () {
    domEvents($('body'))
})

/**
 * DOM Events
 * 
 * @param {Element} container 
 */
window.domEvents = function (container) {
    $('input, textarea, select', container).each(function () {
        $(this).on('keydown change', function () {
            inputChange(this)
        })

        if ($(this).data('callback') && typeof (window[$(this).data('callback')]) === 'function') {
            window[$(this).data('callback')](this);
        }
    })

    if ($('.form', container).length) {
        $('.form', container).each(function () {
            $(this).on('submit', function () {
                helpers.showLoading('input[type=submit], button[type=submit]', this)
            })
        })
    }

    if ($('textarea.rich-text').length) {
        $('textarea.rich-text').each(function () {
            var tinyMceConfig = {
                selector: 'textarea.rich-text',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'table emoticons template paste help'
                ],
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
                    'bullist numlist outdent indent | link image | print preview media fullpage | ' +
                    'forecolor backcolor emoticons | help',
                menu: {
                    favs: { title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons' }
                },
                menubar: 'favs file edit view insert format tools table help',
                color_map: [
                    '333333', 'Black',
                    '293d49', 'Gray',
                    'bbcad2', 'Silver',
                    'f2f3f4', 'White',
                    'e1252b', 'Red',
                    'efa15d', 'Yellow',
                    '67ad68', 'Green',
                    '85b2df', 'Blue',
                    'ffffff', 'Super White',
                    '000000', 'Super Black'
                ]
            }

            if ($(this).data('styles')) {
                tinyMceConfig.content_css = $(this).data('styles')
            }

            tinymce.init(tinyMceConfig)
        })
    }
}

/**
 * Input Change
 * 
 * @param {Element} object 
 */
window.inputChange = function (object) {
    var obj = $(object)

    if (obj.hasClass('wrong')) obj.removeClass('wrong')

    if (obj.attr('type') != 'checkbox' && obj.next().hasClass('form-input-warning')) {
        obj.next().remove()
    }

    if (obj.attr('type') == 'checkbox' && obj.parent().next().hasClass('form-input-warning')) {
        obj.parent().next().remove()
    }

    if (obj.data('callback') && typeof (window[obj.data('callback')]) === 'function') {
        window[obj.data('callback')](object)
    }
}
