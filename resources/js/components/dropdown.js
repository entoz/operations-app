$.fn.initDropdown = function () {
    var el = this
    el.functions = {}

    $(el).addClass('relative text-left transition-backkground duration-300 hover:bg-gray-light inline-block')

    $('> ul', el).addClass('absolute hidden w-auto right-0 top-full small bg-gray-light');

    $('> a', el).append('<div class="dropdown-expander inline-block ml-2"><span class="material-icons transition-transform duration-300 ">expand_more</span></div>').addClass('block p-6')

    el.functions.reset = function () {
        if ($(el).hasClass('active')) {
            $(el).removeClass('active').removeClass('bg-gray-light')
        }
    }

    $('> a', el).on('click', function () {
        if ($(el).hasClass('active')) {
            el.functions.reset()
        } else {
            $(this).parent().addClass('active').addClass('bg-gray-light')
        }

        return false;
    })

    $(window).on('click', function (event) {
        el.functions.reset()
    })
}

$(function () {
    if ($('.dropdown').length) {
        $('.dropdown').initDropdown()
    }
})