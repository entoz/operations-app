window.preloader = {}

window.preloader.count = 0;

window.preloader.show = function() {
    $('#preloader').show()
}

window.preloader.hide = function() {
    $('#preloader').hide()
}

$(function() {
    window.preloader.hide()
})

window.addEventListener('beforeunload', function(e) {
    window.preloader.show()
});