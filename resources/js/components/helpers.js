window.helpers = {}

/**
 * Show loading
 *
 * @param {String} selector
 * @param {Element} container
 */
window.helpers.showLoading = function (selector, container) {
    container = typeof container !== 'undefined' ? container : document

    $(selector, container).attr('disabled', true).addClass('loading')
}

/**
 * Hide loading
 *
 * @param {String} selector
 * @param {Element} container
 */
window.helpers.hideLoading = function (selector, container) {
    container = typeof container !== 'undefined' ? container : document

    $(selector, container).attr('disabled', false).removeClass('loading')
}

/**
 * Clear messages
 *
 * @param {Element} container
 */
window.helpers.clearMessages = function (container) {
    // Container
    container = typeof container !== 'undefined' ? container : document

    // Messages
    $('.message, .input-message', container).each(function () {
        $(this).remove()
    })
}

/**
 * Show message
 *
 * @param {String} text
 * @param {String} selector
 * @param {Element} container
 * @param {String} type
 */
window.helpers.showMessage = function (text, selector, type, container) {
    // Check parameters
    text = typeof text !== 'undefined' ? text : ''
    selector = typeof selector !== 'undefined' ? selector : '#message-body'
    container = typeof container !== 'undefined' ? container : document
    type = typeof type !== 'undefined' ? type : 'success'

    // Create message
    var message = document.createElement('div')

    // Add classes
    $(message).addClass('message py-3 px-5 mb-6 border rounded')
    $(message).attr('hidden', true)

    // Switch type
    switch (type) {
        case 'error':
            text = '<span class="material-icons mr-4">error</span>' + text;
            $(message).addClass('text-red border-red')

            break
        case 'warning':
            text = '<span class="material-icons mr-4">info</span>' + text;
            $(message).addClass('text-orange border-orange')

            break
        default:
            text = '<span class="material-icons mr-4">warning</span>' + text;
            $(message).addClass('text-green border-green')
    }

    // Append text to the message
    $(message).append(text)

    // Append message to the container
    $(selector, container).append(message)

    // Show message
    $(message).fadeIn(300)
}

/**
 * Show input message
 *
 * @param {Element} obj
 * @param {String} message
 */
window.helpers.showInputMessage = function (obj, message) {
    // Check input icon
    obj = obj.parent()

    // Remove old message
    if (obj.next().hasClass('input-message')) obj.next().remove()

    // Add input message
    obj.after(
        '<div class="input-message x-small red-bg super-white">' +
        message +
        '</div>'
    )
}

/**
 *
 * @param {String} field
 * @param {String} error
 * @param {Element} container
 *
 * @return {boolean}
 */
window.helpers.checkFormField = function (field, error, container) {
    // Check field DOM
    if (
        !$(field, container).length ||
        ($(field, container).length &&
            ($(field, container).prop('disabled') ||
                $(field, container).prop('readonly')))
    ) {
        return true
    }

    // Check if field is empty
    if (
        $(field, container)
            .val()
            .trim() === ''
    ) {
        $(field, container)
            .addClass('wrong-input')

        // Check rrror
        if (typeof error !== 'undefined') {
            showMessage(error, 'error', 'none', container)
        }

        return false
    }

    return true
}
