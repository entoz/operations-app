window.modal = {}

$.fn.initModal = function () {
    var el = this;

    $('.modal-close', el).on('click', function () {
        modal.hide()

        return false
    })

    $('.modal-mask', el).on('click', function () {
        modal.hide()
    })

    $('.modal-wrapper', el).on('click', function (event) {
        event.stopPropagation()
    })
}

window.modal.show = function (url) {
    if (typeof url === 'undefined') return;
    if (typeof customClass === 'undefined') customClass = false

    var scrollTop = $(document).scrollTop()

    $('body').addClass('overflow-hidden').scrollTop(scrollTop)
    //$('#wrapper').addClass('filter').addClass('blur-sm')
    $('.modal').removeClass('hidden').data('scroll-top', scrollTop)
    $('.modal-wrapper').addClass('loading')
    $('.modal-close').addClass('hidden')
    $('.modal-content').html('')

    $.get(url, function (data) {
        $('.modal-wrapper').removeClass('loading')
        $('.modal-close').removeClass('hidden')
        $('.modal-content').html(data)

        domEvents($('.modal'))
    })
}

window.modal.hide = function () {
    $('body').removeClass('overflow-hidden').scrollTop(0)
    $(document).scrollTop($('.modal').data('scroll-top'))
    //$('#wrapper').removeClass('filter').removeClass('blur-sm')
    $('.modal').addClass('hidden')
    $('.modal-content').html('')
}
