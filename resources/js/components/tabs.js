$.fn.initTabs = function () {
    var el = this
    el.functions = {}

    el.functions.reset = function () {
        $('> .tab-nav > button', el).removeClass('active')
        $('> .tab-content > div', el).addClass('hidden')
    }

    el.functions.set = function (index) {
        $('> .tab-nav > button:nth-child(' + index + ')', el).addClass('active')
        $('> .tab-content > div:nth-child(' + index + ')', el).removeClass('hidden')
    }

    $('> .tab-nav > button', el).on('click', function () {
        if (!$(this).hasClass('active')) {
            el.functions.reset()

            el.functions.set($(this).index() + 1)
        }

        return false;
    })
    
    if (!$('> .tab-nav > button.active', el).length) {
        el.functions.set(1)
    }
}

$(function () {
    if ($('.tabs').length) {
        $('.tabs').initTabs()
    }
})