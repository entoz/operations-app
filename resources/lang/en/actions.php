<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Action Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the action library
    |
    */

    'save' => 'Save',
    'cancel' => 'Cancel',
        
];
