<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the messages library
    |
    */

    'title' => 'Operations App',
    'owner' => 'Giorgi Omiadze',
    'description' => 'A Laravel driven administration panel.',
    'rights' => '© All rights reserved',
    'dashboard' => 'Dashboard',
    'settings' => 'Settings',
    'entry-types' => 'Entry Types',
    'add-new-entry-type' => 'Add new entry type',
    'options' => 'Options',
    'add-new-option' => 'Add new option',
    'fields' => 'Fields',
    'add-new-field' => 'Add new field',
    'languages' => 'Languages',
    'add-new-language' => 'Add new language',
    'add' => 'Add',
    'edit' => 'Edit',
    'slug' => 'Slug',
    'locale' => 'Locale',
    'name' => 'Name',
    'description' => 'Description',
    'full-name' => 'Full name',
    'primary' => 'Primary',
    'option-value' => 'Value',
    'option-value-type' => 'Value type',
    'required' => 'Required',
    'type' => 'Type',
    'translate' => 'Translate value',
    'default' => 'Default value',
    'hierarchical' => 'Hierarchical',
    'sortable' => 'Sortable',
    'searchable' => 'Searchable',
    'string' => 'String',
    'number' => 'Number',
    'text' => 'Text',
    'rich-text' => 'Rich text',
    'json' => 'JSON',
    'boolean' => 'Boolean',
    'regex' => 'Regular expression check',
    'data' => 'Data',
    'select' => 'Select',
    'related-fields' => 'Related fields',
    'related-fields-not-found' => 'No related fields available',
    'entries' => 'Entries',
    'add-new-entry' => 'Add new entry',
    'delete' => 'Delete',
    'cancel' => 'Cancel',
    'template' => 'Template',
    'parent' => 'Parent',
    'published' => 'Published',
    'published-at' => 'Publish Date',
    'ord' => 'Order',
    'edit-order' => 'Edit Order',

];
