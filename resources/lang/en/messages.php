<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the messages library
    |
    */

    'error' => 'An error occurred, please try again later.',
    '%s-not-found' => '%s not found',
    '%s-added' => '%s added successfully',
    '%s-saved' => '%s saved successfully',
    'internal-server-error' => 'Internal server error',
    'operation-completed' => 'Operation completed',
    'not-found' => 'Not found',
    'the-page-you-are-looking-for-not-found' => 'The page you\'re looking for not found.',
    'please-check-url-address' => 'Please, check the URL address',
    'delete?' => 'Are you sure, that you want to delete entry?',

];
