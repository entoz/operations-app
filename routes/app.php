<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EntryController;
use App\Http\Controllers\EntryTypeController;
use App\Http\Controllers\FieldController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ModalController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\SortController;

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register App routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "app" middleware group.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('fields', FieldController::class);

Route::resource('options', OptionController::class);

Route::resource('languages', LanguageController::class);

Route::resource('entry-types', EntryTypeController::class);

Route::resource('entry-types.entries', EntryController::class, ['names' => [
    'index' => 'entries.index',
    'store' => 'entries.store',
    'create' => 'entries.create',
    'show' => 'entries.show',
    'update' => 'entries.update',
    'destroy' => 'entries.destroy',
    'edit' => 'entries.edit',
]]);

Route::get('/modal/{view}/{route}/{id}', [ModalController::class, 'viewRouteId'])
    ->name('modal-view-route-id');

Route::get('/modal/{view}/entry-types/{entry_type_id}/{route}/{id}', [ModalController::class, 'viewEntryTypeRouteId'])
    ->name('modal-view-entry-type-route-id');

Route::put('/sort/{route}/{parent}', [SortController::class, 'sortRoute'])->name('sort');
