<?php

namespace App\Helpers;

use App\Models\Language;
use Illuminate\Http\Request;

class RequestHelper
{
    /**
     * Generate Data
     *
     * Gets Validated Request and combines with model attributes
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Array  $attributes
     * @return Array
     */
    public static function generateData(Request $request, array $attributes)
    {
        $data = $request->validated();

        foreach ($attributes as $key => $value) {
            if (isset($value['type']) && $value['type'] == 'boolean') {
                $data[$key] = $request->has($key);
            }
        }

        return $data;
    }

    /**
     * Map Meta Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Array $fields
     * @return Array
     */
    public static function mapMetaData(Request $request, array $fields)
    {
        $languages = Language::get();
        $data = [];
        $validated = [];
        $validations = [];
        
        foreach ($languages as $language) {
            foreach ($fields as $field) {
                $validations['meta.' . $language->slug . '.' . $field['slug']] = [];

                if ($field['required']) {
                    $validations['meta.' . $language->slug . '.' . $field['slug']][] = 'required';
                }
            }
        }
        
        $validated = $request->validate($validations);
        
        /*
        foreach ($languages as $language) {
            foreach ($fields as $field) {
                if ($field['type'] == 'boolean') {
                    $data['meta'][$language->slug][$field['slug']] = $request->has('meta.' . $language->slug . '.' . $field['slug']);
                }
            }
        }
        */
        
        if (isset($validated['meta'])) {
            foreach ($validated['meta'] as $lang => $meta) {
                foreach ($meta as $key => $value) {
                    $data[] = [
                        'language' => $lang,
                        'meta_field' => $key,
                        'meta_value' => $value,
                    ];
                }
            }
        }
        
        return $data;
    }
}
