<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class MessageHelper
{
    /**
     * Flash Action Message
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $target
     * @param  String  $action
     * @return void
     */
    public static function flashActionMessage(Request $request, String $target, String $action)
    {
        $request->session()->flash('message', sprintf(__('messages.%s-' . $action), $target));
    }

    /**
     * Flash Error Message
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public static function flashErrorMessage(Request $request)
    {
        $request->session()->flash('warning', __('messages.error'));
    }
}
