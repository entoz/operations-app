<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class StringHelper
{
    /**
     * Route to Name
     *
     * @param  String  $route
     * @return String
     */
    public static function routeToName(String $route)
    {
        return ucfirst(Str::singular(Str::slug($route, ' ')));
    }

    /**
     * Route to Model Name
     *
     * @param  String  $route
     * @return String
     */
    public static function routeToModelName(String $route)
    {
        return 'App\\Models\\' . ucFirst(Str::singular($route));
    }

    /**
     * Route to Parameter Name
     *
     * @param  String  $route
     * @return String
     */
    public static function routeToParamName(String $route)
    {
        return Str::singular(Str::slug($route, '_'));
    }
}
