<?php

namespace App\View\Components;

use App\Models\Entry;
use App\Models\EntryType;
use App\Models\Language;
use Illuminate\View\Component;

class MetaFields extends Component
{
    /**
     * Entry
     *
     * @var Entry
     */
    private $entry;

    /**
     * Entry Type
     *
     * @var EntryType
     */
    private $entryType;

    /**
     * Data
     *
     * @var array
     */
    private $languages = [];

    /**
     * Meta
     *
     * @var array
     */
    private $meta = [];

    /**
     * Create a new component instance.
     *
     * @param Entry $entry
     * @param EntryType $entryType
     * @return void
     */
    public function __construct(Entry $entry, EntryType $entryType)
    {
        $this->entry = $entry;
        $this->entryType = $entryType;
        $this->languages = Language::all();

        if ($this->entry && $this->entry->meta) {
            foreach ($this->entry->meta as $meta) {
                $this->meta[$meta->language][$meta->meta_field] = $meta->meta_value;
            }
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.meta-fields', [
            'languages' => $this->languages,
            'fields' => $this->entryType->fields,
            'meta' => $this->meta,
        ]);
    }
}
