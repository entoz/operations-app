<?php

namespace App\View\Components;

use App\Models\EntryType;
use Illuminate\View\Component;

class EntryTypeMenu extends Component
{
    /**
     * Entry
     *
     * @var EntryType
     */
    private $entryType;

    /**
     * Create a new component instance.
     * 
     * @param EntryType $entryType
     * @return void
     */
    public function __construct($entryType)
    {
        $this->entryType = $entryType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.entry-type-menu', [
            'entryType' => $this->entryType,
            'entryTypes' => EntryType::orderBy('ord')->get(),
        ]);
    }
}
