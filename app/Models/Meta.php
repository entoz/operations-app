<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language', 'meta_field', 'meta_value'
    ];

    /**
     * Metaable
     *
     * @return array
     */
    public function metaable()
    {
        return $this->morphTo();
    }
}
