<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'entry_type_id', 'template', 'parent', 'ord', 'published', 'published_at'
    ];

    /**
     * Additional fields to threat as Carbon instances
     *
     * @var array
     */
    protected $dates = ['published_at'];
    
    /**
     * The attributes that are editable.
     *
     * @var array
     */
    public const ATTRIBUTES = [
        'slug' => [
            'type' => 'string',
        ],
        'entry_type_id' => [
            'type' => 'entry-type-id',
        ],
        'template' => [
            'type' => 'string',
        ],
        'parent' => [
            'type' => 'parent-entry',
        ],
        'ord' => [
            'type' => 'hidden',
        ],
        'published' => [
            'type' => 'boolean',
        ],
        'published_at' => [
            'type' => 'datetime',
        ],
    ];

    /**
     * Scope a query to get entry by slug.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  String $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Scope a query to only include published entries.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1)->where('published_at', '<=', Carbon::now());
    }

    /**
     * Scope a query to only include specified type entries.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  Int  $entry_type_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeType($query, $entry_type_id)
    {
        return $query->where('entry_type_id', $entry_type_id);
    }

    /**
     * Scope a query to only include specified parent entries.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  Int  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeParent($query, $parent)
    {
        return $query->where('parent', $parent);
    }

    /**
     * Meta
     * 
     * @return array
     */
    public function meta()
    {
        return $this->morphMany(Meta::class, 'metaable');
    }

    /**
     * Children
     * 
     * @return array
     */
    public function children()
    {
        return $this->hasMany(Entry::class, 'parent')->orderBy('ord');
    }
}
