<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'locale', 'name', 'full_name', 'ord'
    ];

    /**
     * The attributes that are editable.
     *
     * @var array
     */
    public const ATTRIBUTES = [
        'slug' => [
            'type' => 'string',
        ],
        'locale' => [
            'type' => 'string',
        ],
        'name' => [
            'type' => 'string',
        ],
        'full_name' => [
            'type' => 'string',
        ],
        'ord' => [
            'type' => 'hidden',
        ],
    ];
}
