<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'option_value', 'option_value_type', 'ord'
    ];

    /**
     * The attributes that are editable.
     *
     * @var array
     */
    public const ATTRIBUTES = [
        'slug' => [
            'type' => 'string'
        ],
        'name' => [
            'type' => 'string'
        ],
        'option_value' => [
            'type' => 'string'
        ],
        'option_value_type' => [
            'type' => 'select',
            'data' => ['string','json','boolean'],
        ],
        'ord' => [
            'type' => 'hidden',
        ],
    ];
}
