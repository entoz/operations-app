<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryType extends Model
{
    use HasFactory;

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'hierarchical', 'sortable', 'searchable', 'ord'
    ];

    /**
     * The attributes that are editable.
     *
     * @var array
     */
    public const ATTRIBUTES = [
        'slug' => [
            'type' => 'string',
        ],
        'name' => [
            'type' => 'string',
        ],
        'hierarchical' => [
            'type' => 'boolean',
        ],
        'sortable' => [
            'type' => 'boolean',
        ],
        'searchable' => [
            'type' => 'boolean',
        ],
        'ord' => [
            'type' => 'hidden',
        ],
    ];

    /**
     * The fields that belong to the entry type.
     */
    public function fields()
    {
        return $this->belongsToMany(Field::class)->withTimestamps();
    }
}
