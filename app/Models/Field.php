<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'description', 'type', 'translate', 'required', 'default', 'regex', 'data', 'ord'
    ];

    /**
     * The attributes that are editable.
     *
     * @var array
     */
    public const ATTRIBUTES = [
        'slug' => [
            'type' => 'string',
        ],
        'name' => [
            'type' => 'string',
        ],
        'description' => [
            'type' => 'text',
        ],
        'type' => [
            'type' => 'select',
            'data' => ['string', 'number', 'text', 'rich-text', 'json', 'boolean', 'select'],
            'callback' => 'fieldTypeCallback'
        ],
        'translate' => [
            'type' => 'boolean',
        ],
        'required' => [
            'type' => 'boolean',
        ],
        'default' => [
            'type' => 'boolean',
        ],
        'regex' => [
            'type' => 'string',
        ],
        'data' => [
            'type' => 'text',
        ],
        'ord' => [
            'type' => 'hidden',
        ],
    ];

    /**
     * The entry types that belong to the field.
     */
    public function entryTypes()
    {
        return $this->belongsToMany('App\EntryType')->withTimestamps();
    }
}
