<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FieldRequest;
use App\Models\Field;
use App\Helpers\RequestHelper;
use App\Helpers\MessageHelper;
use App\Helpers\StringHelper;

class FieldController extends Controller
{
    /**
     * Name of the route
     *
     * @var string
     */
    private $route = 'fields';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('list', [
            'route' => $this->route,
            'items' => Field::orderBy('ord')->get(),
            'sortable' => true,
            'hierarchical' => false,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item', [
            'route' => $this->route,
            'item' => false,
            'attributes' => Field::ATTRIBUTES,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FieldRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FieldRequest $request)
    {
        $data = RequestHelper::generateData($request, Field::ATTRIBUTES);

        $data['ord'] = Field::max('ord') + 1;

        if (Field::create($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'added');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function show(Field $field)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function edit(Field $field)
    {
        return view('item', [
            'route' => $this->route,
            'item' => $field,
            'attributes' => Field::ATTRIBUTES,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\FieldRequest  $request
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function update(FieldRequest $request, Field $field)
    {
        $data = RequestHelper::generateData($request, Field::ATTRIBUTES);

        if ($field->update($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'saved');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', [$this->route => $field]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function destroy(Field $field)
    {
        $field->delete();
    }
}
