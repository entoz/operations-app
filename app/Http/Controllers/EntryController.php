<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EntryRequest;
use App\Models\Entry;
use App\Models\EntryType;
use App\Helpers\RequestHelper;
use App\Helpers\MessageHelper;
use App\Helpers\StringHelper;
use Illuminate\Support\Facades\DB;

class EntryController extends Controller
{
    /**
     * Name of the route
     *
     * @var string
     */
    private $route = 'entries';

    /**
     * Display a listing of the resource.
     *
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function index(EntryType $entry_type)
    {
        return view('list', [
            'route' => $this->route,
            'entryType' => $entry_type,
            'items' => Entry::type($entry_type->id)->parent(0)->orderBy('ord')->get(),
            'sortable' => $entry_type->sortable,
            'hierarchical' => $entry_type->hierarchical,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function create(EntryType $entry_type)
    {
        return view('item', [
            'route' => $this->route,
            'entryType' => $entry_type,
            'item' => false,
            'attributes' => Entry::ATTRIBUTES,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EntryRequest  $request
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function store(EntryRequest $request, EntryType $entry_type)
    {
        $data = RequestHelper::generateData($request, Entry::ATTRIBUTES);

        $data['ord'] = Entry::type($entry_type->id)->parent($request->get('parent'))->max('ord') + 1;

        DB::beginTransaction();

        if ($entry = Entry::create($data)) {
            $entry->meta()->createMany(RequestHelper::mapMetaData($request, $entry_type->fields->toArray()));

            DB::commit();

            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'added');

            return redirect()->route($this->route . '.index', ['entry_type' => $entry_type]);
        }

        DB::rollBack();

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', ['entry_type' => $entry_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EntryType  $entry_type
     * @param  \App\Entry $entry
     * @return \Illuminate\Http\Response
     */
    public function show(EntryType $entry_type, Entry $entry)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EntryType  $entry_type
     * @param  \App\Entry $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(EntryType $entry_type, Entry $entry)
    {
        return view('item', [
            'route' => $this->route,
            'entryType' => $entry_type,
            'item' => $entry,
            'attributes' => Entry::ATTRIBUTES,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EntryRequest  $request
     * @param  \App\EntryType  $entry_type
     * @param  \App\Entry $entry
     * @return \Illuminate\Http\Response
     */
    public function update(EntryRequest $request, EntryType $entry_type, Entry $entry)
    {
        $data = RequestHelper::generateData($request, Entry::ATTRIBUTES);

        $entry = Entry::where('id', $entry->id)->first();

        if ($entry->update($data)) {
            $entry->meta()->delete();

            $entry->meta()->createMany(RequestHelper::mapMetaData($request, $entry_type->fields->toArray()));

            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'saved');

            return redirect()->route($this->route . '.index', ['entry_type' => $entry_type]);
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', ['entry_type' => $entry_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EntryType  $entry_type
     * @param  \App\Entry $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(EntryType $entry_type, Entry $entry)
    {
        $entry->delete();
    }
}
