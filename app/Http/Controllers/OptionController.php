<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\OptionRequest;
use App\Models\Option;
use App\Helpers\RequestHelper;
use App\Helpers\MessageHelper;
use App\Helpers\StringHelper;

class OptionController extends Controller
{
    /**
     * Name of the route
     *
     * @var string
     */
    private $route = 'options';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('list', [
            'route' => $this->route,
            'items' => Option::orderBy('ord')->get(),
            'sortable' => true,
            'hierarchical' => false,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item', [
            'route' => $this->route,
            'item' => false,
            'attributes' => Option::ATTRIBUTES,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OptionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OptionRequest $request)
    {
        $data = RequestHelper::generateData($request, Option::ATTRIBUTES);

        $data['ord'] = Option::max('ord') + 1;

        if (Option::create($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'added');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        return view('item', [
            'route' => $this->route,
            'item' => $option,
            'attributes' => Option::ATTRIBUTES,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OptionRequest  $request
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(OptionRequest $request, Option $option)
    {
        $data = RequestHelper::generateData($request, Option::ATTRIBUTES);

        if ($option->update($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'saved');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', [$this->route => $option]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy(Option $option)
    {
        $option->delete();
    }
}
