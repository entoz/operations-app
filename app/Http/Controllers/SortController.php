<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\SortRequest;
use App\Helpers\StringHelper;
use Illuminate\Support\Facades\DB;

class SortController extends Controller
{
    /**
     * Sort Route
     *
     * @param SortRequest $request
     * @param String $route
     * @param Int $parent
     * @return \Illuminate\Http\Response
     */
    public function sortRoute(SortRequest $request, String $route, Int $parent)
    {
        $modelName = StringHelper::routeToModelName($route);

        if ($order = $request->validated()) {
            DB::beginTransaction();

            foreach ($order['ord'] as $index => $id) {
                $item = $modelName::where('id', $id)->first();

                $data = ['ord' => $index + 1];

                if (in_array('parent', $item->getFillable())) {
                    $data['parent'] = $parent;
                }

                if (!$item->update($data)) {
                    DB::rollBack();

                    return response()->json(['message' => __('messages.internal-server-error')], 500);
                }
            }

            DB::commit();
        }

        return response()->json(['message' => __('messages.operation-completed')], 200);
    }
}
