<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\StringHelper;

class ModalController extends Controller
{
    /**
     * View Route ID
     * 
     * @param String $view
     * @param String $route
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function viewRouteId(String $view, String $route, Int $id)
    {
        $modelName = StringHelper::routeToModelName($route);

        $item = $modelName::where('id', $id)->first();

        return view('modal.' . $view, [
            'route' => route($route . '.destroy', [StringHelper::routeToParamName($route) => $item]),
            'name' => ucFirst($route) . ' / ' . $item->name,
            'id' => $item->id
        ]);
    }

    /**
     * View Entry Type Route ID
     * 
     * @param String $view
     * @param Int $entry_type_id
     * @param String $route
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function viewEntryTypeRouteId(String $view, Int $entry_type_id, String $route, Int $id)
    {
        $modelName = StringHelper::routeToModelName($route);

        $item = $modelName::where('id', $id)->first();

        return view('modal.' . $view, [
            'route' => route($route . '.destroy', ['entry_type' => $entry_type_id, StringHelper::routeToParamName($route) => $item]),
            'name' => StringHelper::routeToName($route) . ' / ' . $item->slug,
            'entryTypeId' => $entry_type_id,
            'id' => $item->id
        ]);
    }
}
