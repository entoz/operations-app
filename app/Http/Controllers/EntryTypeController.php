<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EntryTypeRequest;
use App\Models\EntryType;
use App\Models\Field;
use App\Helpers\RequestHelper;
use App\Helpers\MessageHelper;
use App\Helpers\StringHelper;

class EntryTypeController extends Controller
{
    /**
     * Name of the route
     *
     * @var string
     */
    private $route = 'entry-types';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('list', [
            'route' => $this->route,
            'items' => EntryType::orderBy('ord')->get(),
            'sortable' => true,
            'hierarchical' => false,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item', [
            'route' => $this->route,
            'item' => false,
            'attributes' => EntryType::ATTRIBUTES,
            'availableFields' => Field::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EntryTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntryTypeRequest $request)
    {
        $data = RequestHelper::generateData($request, EntryType::ATTRIBUTES);

        $data['ord'] = EntryType::max('ord') + 1;

        if ($entry_type = EntryType::create($data)) {
            $entry_type->fields()->sync($request->get('related-field'));

            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'added');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function show(EntryType $entry_type)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function edit(EntryType $entry_type)
    {
        return view('item', [
            'route' => $this->route,
            'item' => $entry_type,
            'attributes' => EntryType::ATTRIBUTES,
            'availableFields' => Field::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EntryTypeRequest  $request
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function update(EntryTypeRequest $request, EntryType $entry_type)
    {
        $data = RequestHelper::generateData($request, EntryType::ATTRIBUTES);

        if ($entry_type->update($data)) {
            $entry_type->fields()->sync($request->get('related-field'));

            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'saved');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', [$this->route => $entry_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EntryType  $entry_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(EntryType $entry_type)
    {
        $entry_type->delete();
    }
}
