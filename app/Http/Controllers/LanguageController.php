<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LanguageRequest;
use App\Models\Language;
use App\Helpers\RequestHelper;
use App\Helpers\MessageHelper;
use App\Helpers\StringHelper;

class LanguageController extends Controller
{
    /**
     * Name of the route
     *
     * @var string
     */
    private $route = 'languages';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('list', [
            'route' => $this->route,
            'items' => Language::orderBy('ord')->get(),
            'sortable' => true,
            'hierarchical' => false,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item', [
            'route' => $this->route,
            'item' => false,
            'attributes' => Language::ATTRIBUTES,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\LanguageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LanguageRequest $request)
    {
        $data = RequestHelper::generateData($request, Language::ATTRIBUTES);

        $data['ord'] = Language::max('ord') + 1;

        if (Language::create($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'added');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        return view('item', [
            'route' => $this->route,
            'item' => $language,
            'attributes' => Language::ATTRIBUTES,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\LanguageRequest  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(LanguageRequest $request, Language $language)
    {
        $data = RequestHelper::generateData($request, Language::ATTRIBUTES);

        if ($language->update($data)) {
            MessageHelper::flashActionMessage($request, StringHelper::routeToName($this->route), 'saved');

            return redirect()->route($this->route . '.index');
        }

        MessageHelper::flashErrorMessage($request);

        return redirect()->route($this->route . '.index', [$this->route => $language]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        $language->delete();
    }

    /**
     * Sort
     *
     * @param  Array  $order
     * @return \Illuminate\Http\Response
     */
    public function sort(array $order)
    {
        dd($order);
    }
}
