<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'alpha_dash',
                'unique:options,slug' . ($this->has('id') ? ',' . $this->id : '')
            ],
            'name' => [
                'required'
            ],
            'option_value' => [
                'required'
            ],
            'option_value_type' => [
                'required'
            ],
        ];
    }
}
