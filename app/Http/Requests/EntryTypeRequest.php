<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntryTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'alpha_dash',
                'unique:entry_types,slug' . ($this->has('id') ? ',' . $this->id : '')
            ],
            'name' => [
                'required'
            ],
            'hierarchical' => [
                'boolean'
            ],
            'sortable' => [
                'boolean'
            ],
            'searchable' => [
                'boolean'
            ],
        ];
    }
}
