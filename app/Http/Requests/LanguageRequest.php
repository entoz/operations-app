<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LanguageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'alpha_dash',
                'size:2',
                'unique:languages,slug' . ($this->has('id') ? ',' . $this->id : '')
            ],
            'locale' => [
                'required',
                'regex:/^[a-z]{2}_[A-Z]{2}$/i'
            ],
            'name' => [
                'required'
            ],
            'full_name' => [
                'required'
            ],
        ];
    }
}
