<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'alpha_dash',
                'unique:fields,slug' . ($this->has('id') ? ',' . $this->id : '')
            ],
            'name' => [
                'required'
            ],
            'description' => [
                'nullable'
            ],
            'type' => [
                'required'
            ],
            'translate' => [
                'boolean'
            ],
            'required' => [
                'boolean'
            ],
            'default' => [
                'boolean'
            ],
            'regex' => [
                'nullable',
            ],
            'data' => [
                'nullable'
            ],
        ];
    }
}
