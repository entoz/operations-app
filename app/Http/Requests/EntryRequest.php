<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'alpha_dash',
                'unique:entries,slug' . ($this->has('id') ? ',' . $this->id : '')
            ],
            'entry_type_id' => [
                'required'
            ],
            'template' => [
                'nullable'
            ],
            'parent' => [
                'required'
            ],
            'published' => [
                'boolean'
            ],
            'published_at' => [
                'date'
            ],
        ];
    }
}
