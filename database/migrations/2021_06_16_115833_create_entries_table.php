<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->foreignId('entry_type_id')->constrained()->onDelete('cascade');
            $table->string('template')->nullable();
            $table->integer('parent')->default(0);
            $table->integer('ord')->default(0);
            $table->integer('published')->default(0);
            $table->timestamp('published_at')->useCurrent();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
