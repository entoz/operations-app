<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // English
        Language::create([
            'slug' => 'en',
            'locale' => 'en_US',
            'name' => 'English',
            'full_name' => 'English (US)',
            'ord' => 1,
        ]);

        // Georgian
        Language::create([
            'slug' => 'ka',
            'locale' => 'ka_GE',
            'name' => 'ქართული',
            'full_name' => 'Georgian (KA)',
            'ord' => 2,
        ]);

        // You can create dump data of the languages by calling a factory
        //Language::factory()->count(1)->create();
    }
}
