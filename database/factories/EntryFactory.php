<?php

namespace Database\Factories;

use App\Models\Entry;
use App\Models\EntryType;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Entry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $entryTypes = EntryType::all()->pluck('id');

        return [
            'slug' => $this->faker->unique()->word,
            'entry_type_id' => $this->faker->randomElement($entryTypes),
            'published' => $this->faker->randomElement([0,1]),
            'published_at' => $this->faker->dateTime(),
        ];
    }
}
