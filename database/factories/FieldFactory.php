<?php

namespace Database\Factories;

use App\Models\Field;
use Illuminate\Database\Eloquent\Factories\Factory;

class FieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Field::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'slug' => $this->faker->unique()->word,
            'type' => $this->faker->randomElement(['string', 'text', 'rich-text', 'number', 'boolean', 'image']),
            'name' => ucFirst($this->faker->word),
            'description' => $this->faker->sentence(),
            'translate' => $this->faker->boolean,
            'required' => $this->faker->boolean,
            'default' => $this->faker->boolean,
            'regex' => '',
            'data' => '',
        ];
    }
}
