<?php

namespace Database\Factories;

use App\Models\EntryType;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntryTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EntryType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'slug' => $this->faker->unique()->word,
            'name' => ucFirst($this->faker->word),
            'hierarchical' => $this->faker->boolean,
            'sortable' => $this->faker->boolean,
            'searchable' => $this->faker->boolean,
        ];
    }
}
