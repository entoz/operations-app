module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: {
        DEFAULT: '#f2f3f4',
        'super': '#ffffff',
        'x-light': '#f9fafb',
        'light': '#f5f6fa',
        'dark': '#ebecee',
        'x-dark': '#e9e9ea',
      },
      silver: {
        DEFAULT: '#bbcad2',
        'x-light': '#dbe3e7',
        'light': '#ccd7dd',
        'dark': '#acbec8',
        'x-dark': '#9db4c0',
      },
      gray: {
        DEFAULT: '#293d49',
        'x-light': '#3c596a',
        'light': '#324b59',
        'dark': '#202f39',
        'x-dark': '#172228',
      },
      red: {
        DEFAULT: '#e1252b',
        'x-light': '#e95157',
        'light': '#e63940',
        'dark': '#d12028',
        'x-dark': '#ba2027',
      },
      blue: {
        DEFAULT: '#85b2df',
        'x-light': '#adcbe7',
        'light': '#9bbee0',
        'dark': '#71a3d7',
        'x-dark': '#6098d1',
      },
      yellow: {
        DEFAULT: '#efa15d',
        'x-light': '#f4bc8b',
        'light': '#f1ae74',
        'dark': '#ed9446',
        'x-dark': '#ea862f',
      },
      green: {
        DEFAULT: '#67ad68',
        'x-light': '#85bf88',
        'light': '#76b779',
        'dark': '#58a159',
        'x-dark': '#4d9150',
      },
      black: {
        DEFAULT: '#333333',
        'super': '#000000',
        'x-light': '#555555',
        'light': '#444444',
        'dark': '#222222',
        'x-dark': '#111111',
      },
    },
    boxShadow: {
      DEFAULT: '0px 0px 20px 0px rgba(100, 100, 100, 0.1)',
      none: 'none',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
